// ==UserScript==
// @name         洛谷广告屏蔽
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  屏蔽洛谷的广告
// @author       songhongyi
// @match        https://www.luogu.com.cn/problem/*
// @match        https://www.luogu.com.cn/contest/*
// @grant        unsafeWindow
// ==/UserScript==

(function() {
    'use strict';
    var document=unsafeWindow.document;
    setTimeout(function()
    {
        var buttons =document.getElementsByClassName("close lfe-form-sz-small")[0];
        if (buttons)
        {
            buttons.click();
            console.log("广告已经消除");
        }
    },500)
    // Your code here...
})();