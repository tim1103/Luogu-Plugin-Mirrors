// ==UserScript==
// @name         洛谷粉丝统计
// @namespace    http://tampermonkey.net/
// @version      1.0.2
// @description  统计粉丝数，感谢 @鏡音リン 的代码授权
// @author       rui_er
// @match        https://www.luogu.com.cn
// @grant        none
// ==/UserScript==

(function() {
$('document').ready(function(){setTimeout(function () {
    $sidebar = $('#app-old .lg-index-content .lg-right.am-u-lg-3');
    $firstele = $($sidebar.children()[0]);
    $finder = $(`
      <div class="lg-article" id="search-user-form">
        <h2>粉丝统计器</h2>
        <script>
f_uid = parseInt(localStorage.calcUid);
if(f_uid == undefined) {
    f_uid = 122461;
}
localStorage.calcUid = f_uid;
var f_cnt = 1;
var f_users = [];
function f_request(url) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);
    xhr.send(null);
    if (xhr.status != 200) return;
    return JSON.parse(xhr.response);
}
function f_work() {
    console.log("page: "+f_cnt);
    var re = f_request('https://www.luogu.com.cn/fe/api/user/followers?user='+f_uid+'&page='+f_cnt);
    if (re) {
        if (re.users.result.length == 0) {
            clearInterval(f_id);
            f_info();
            return;
        }
        f_users.push.apply(f_users, re.users.result);
        f_cnt++;
    }
}
function f_info() {
    var f_followers = f_users.length;
    var f_color = {Cheater:0,Gray:0,Blue:0,Green:0,Orange:0,Red:0,Purple:0};
    var f_ccflevel = {'0':0,'3':0,'4':0,'5':0,'6':0,'7':0,'8':0,'9':0,'10':0};
    var f_badge = 0;
    var f_banned = 0;
    var f_sum_follower = 0;
    var f_sum_following = 0;
    var f_sum_uid = 0;
    var f_friends = 0;
    for (var i = 0; i < f_followers; i++) {
        var c = f_users[i];
        f_color[c.color]++;
        f_ccflevel[c.ccfLevel]++;
        if (c.badge) f_badge++;
        if (c.isBanned) f_banned++;
        f_sum_follower += c.followerCount;
        f_sum_following += c.followingCount;
        f_sum_uid += c.uid;
        if (c.userRelationship) f_friends++;
    }
    console.log('followers: '+f_followers);
    console.log('color: ');
    for (var x in f_color) {
        console.log('\t'+x+': '+f_color[x]);
    }
    console.log('ccflevel: ');
    for (var y in f_ccflevel) {
        console.log('\t'+y+': '+f_ccflevel[y]);
    }
    console.log('has badge: '+f_badge);
    console.log('banned: '+f_banned);
    console.log('average follower count: '+f_sum_follower/f_followers);
    console.log('average following count: '+f_sum_following/f_followers);
    console.log('average uid: '+f_sum_uid/f_followers);
    console.log('friends: '+f_friends);
    console.log("统计结束");
    alert("统计结束");
}
function calcQuery() {
    console.log("已经开始统计，可能需要一些时间");
    alert("已经开始统计，可能需要一些时间");
    console.log("uid: "+f_uid);
    f_id = setInterval(f_work, 50);
}
function change_usr_calc() {
    localStorage.calcUid = $('[name=change-calc]')[0].value;
    location.href="/";
}
        </script>
        <form id="calc-followers-form">
          <input type="text" class="am-form-field" name="change-calc" placeholder="更换要统计的 uid" autocomplete="off" />
        </form>
        <button class="am-btn am-btn-sm am-btn-primary" id="changer" onclick="change_usr_calc()" style="margin-top:16px">更换</button>
        <button class="am-btn am-btn-sm am-btn-primary lg-right" id="calc" onclick="calcQuery()" style="margin-top:16px">统计</button>
      </div>
    `);
    $finder.insertAfter($firstele);
},500)});
})();